# ma-calvin-liusnando

***

## Name
Web-based Platform for Automated Web User Interfaces Evaluation

## Description
This project aims to facilitate an easier use of WUI metrics and assessment models for automated UI evaluation through a single, unifying web-based platform.

## Installation
* Backend: conda (env. x86-64, version 24.1.2) is used as the virtual environment. You can run the backend easily with conda with this command:
`conda env create -f environment.yml`
  * If you don't have conda installed in your device, please refer to this documentation: https://conda.io/projects/conda/en/latest/user-guide/install/index.html


* Frontend: pnpm is used as the package manager. After navigating to the frontend directory, install the dependency modules with this command:
`pnpm install`. 
  * If you don't have pnpm installed in your device, please refer to this link: https://pnpm.io/installation


* Proxy server (for load balancing, connection limit, rate limit): NGINX is used. Please refer to this documentation for its installation: https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/.
  * A sample `nginx.conf` for load balancing (least connections) with 2 servers running can be found inside the nginx directory. For security reasons, the actually used nginx.conf is not provided in this repository.
  * Read on this guide on how to set up and run nginx: https://nginx.org/en/docs/beginners_guide.html#control

## Usage
* Preliminary preparation: 
  * Supabase is used as the database and file storage provider. Inside the root of the backend package, You should create an .env file containing your `SUPABASE_URL` and `SUPABASE_KEY`. Alternatively, you could use the default database by copying the environment values provided in `.env.example`.
  * You can find more information about Supabase in this link: https://supabase.com/docs

* Backend:
  * Activate your virtual environment as specified in previous <b>Installation</b> section.
  * Run this following command in your terminal: `uvicorn server:app --host 0.0.0.0 --port <port number> --reload`
  * The backend server is now running!

* Frontend:
  * In the root frontend directory, create an .env file, following the example given in its corresponding `.env.example`. The env value of `VITE_PREFIX_URL` depends on where the server is currently running.
  * For instance, if the server is run locally and on port 8000, the value of `VITE_PREFIX_URL` should be `http://localhost:8000/`.
  * To run the frontend in dev mode, use this following command: `pnpm run dev` while you are in the frontend directory
  * Alternatively, you can first build the frontend: `pnpm run build` and then run `pnpm run preview`.

## Support
You can write to calvin.liusnando@s2021.tu-chemnitz.de for any questions pertaining to this project.

## Authors and acknowledgment
Calvin Liusnando (supervised by Dr.-Ing. Sebastian Heil), as part of his thesis work required for the completion of the Master's degree in Web Engineering of TU Chemnitz. 
