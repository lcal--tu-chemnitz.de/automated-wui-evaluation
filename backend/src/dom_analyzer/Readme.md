# DOM Analysis of the HTML Source Code
### A sample output 
URL analyzed: http://info.cern.ch/hypertext/WWW/TheProject.html

```json
{
  "text": "The World Wide Web project World Wide Web The WorldWideWeb (W3) is a wide-area hypermedia information retrieval initiative aiming to give universal access to a large universe of documents. Everything there is online about W3 is linked directly or indirectly to this document, including an executive summary of the project, Mailing lists , Policy , November's W3  news , Frequently Asked Questions . What's out there? Pointers to the world's online information, subjects , W3 servers , etc. Help on the browser you are using Software Products A list of W3 project components and their current state. (e.g. Line Mode ,X11 Viola , NeXTStep , Servers , Tools , Mail robot , Library ) Technical Details of protocols, formats, program internals etc Bibliography Paper documentation on  W3 and references. People A list of some people involved in the project. History A summary of the history of the project. How can I help ? If you would like to support the web.. Getting code Getting the code by anonymous FTP , etc.",
  "elements": [
    {
      "tag": "html",
      "attributes": {},
      "children": [
        "head",
        "body"
      ]
    },
    {
      "tag": "head",
      "attributes": {},
      "children": []
    },
    {
      "tag": "body",
      "attributes": {},
      "children": [
        "header",
        "h1",
        "a",
        "p",
        "dl"
      ]
    },
    {
      "tag": "header",
      "attributes": {},
      "children": [
        "title",
        "nextid"
      ]
    },
    {
      "tag": "title",
      "attributes": {},
      "children": []
    },
    {
      "tag": "nextid",
      "attributes": {
        "n": "55"
      },
      "children": []
    },
    {
      "tag": "h1",
      "attributes": {},
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "0",
        "href": "WhatIs.html"
      },
      "children": []
    },
    {
      "tag": "p",
      "attributes": {},
      "children": [
        "a",
        "a",
        "a",
        "a",
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "24",
        "href": "Summary.html"
      },
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "29",
        "href": "Administration/Mailing/Overview.html"
      },
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "30",
        "href": "Policy.html"
      },
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "34",
        "href": "News/9211.html"
      },
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "41",
        "href": "FAQ/List.html"
      },
      "children": []
    },
    {
      "tag": "dl",
      "attributes": {},
      "children": [
        "dt",
        "dd",
        "dt",
        "dd",
        "dt",
        "dd",
        "dt",
        "dd",
        "dt",
        "dd",
        "dt",
        "dd",
        "dt",
        "dd",
        "dt",
        "dd",
        "dt",
        "dd"
      ]
    },
    {
      "tag": "dt",
      "attributes": {},
      "children": [
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "44",
        "href": "../DataSources/Top.html"
      },
      "children": []
    },
    {
      "tag": "dd",
      "attributes": {},
      "children": [
        "a",
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "45",
        "href": "../DataSources/bySubject/Overview.html"
      },
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "z54",
        "href": "../DataSources/WWW/Servers.html"
      },
      "children": []
    },
    {
      "tag": "dt",
      "attributes": {},
      "children": [
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "46",
        "href": "Help.html"
      },
      "children": []
    },
    {
      "tag": "dd",
      "attributes": {},
      "children": []
    },
    {
      "tag": "dt",
      "attributes": {},
      "children": [
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "13",
        "href": "Status.html"
      },
      "children": []
    },
    {
      "tag": "dd",
      "attributes": {},
      "children": [
        "a",
        "a",
        "a",
        "a",
        "a",
        "a",
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "27",
        "href": "LineMode/Browser.html"
      },
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "35",
        "href": "Status.html#35"
      },
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "26",
        "href": "NeXT/WorldWideWeb.html"
      },
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "25",
        "href": "Daemon/Overview.html"
      },
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "51",
        "href": "Tools/Overview.html"
      },
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "53",
        "href": "MailRobot/Overview.html"
      },
      "children": []
    },
    {
      "tag": "a",
      "attributes": {
        "name": "52",
        "href": "Status.html#57"
      },
      "children": []
    },
    {
      "tag": "dt",
      "attributes": {},
      "children": [
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "47",
        "href": "Technical.html"
      },
      "children": []
    },
    {
      "tag": "dd",
      "attributes": {},
      "children": []
    },
    {
      "tag": "dt",
      "attributes": {},
      "children": [
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "40",
        "href": "Bibliography.html"
      },
      "children": []
    },
    {
      "tag": "dd",
      "attributes": {},
      "children": []
    },
    {
      "tag": "dt",
      "attributes": {},
      "children": [
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "14",
        "href": "People.html"
      },
      "children": []
    },
    {
      "tag": "dd",
      "attributes": {},
      "children": []
    },
    {
      "tag": "dt",
      "attributes": {},
      "children": [
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "15",
        "href": "History.html"
      },
      "children": []
    },
    {
      "tag": "dd",
      "attributes": {},
      "children": []
    },
    {
      "tag": "dt",
      "attributes": {},
      "children": [
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "37",
        "href": "Helping.html"
      },
      "children": []
    },
    {
      "tag": "dd",
      "attributes": {},
      "children": []
    },
    {
      "tag": "dt",
      "attributes": {},
      "children": [
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "48",
        "href": "../README.html"
      },
      "children": []
    },
    {
      "tag": "dd",
      "attributes": {},
      "children": [
        "a"
      ]
    },
    {
      "tag": "a",
      "attributes": {
        "name": "49",
        "href": "LineMode/Defaults/Distribution.html"
      },
      "children": []
    }
  ]
}
```