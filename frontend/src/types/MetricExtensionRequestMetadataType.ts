export interface MetricExtensionRequestMetadataType {
  download_links: string[];
  email_address: string;
}