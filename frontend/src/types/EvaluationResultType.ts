export interface EvaluationResultType {
  metric_id: string;
  results: string[];
}