export interface UrlInputRequestType {
  url: string;
  metrics: string[];
}

export interface FileInputRequestType {
  file: File;
  metrics: string[]
}