import styled from "@emotion/styled";

export const MetricsInformationContentWrap = styled.div`
  padding: 2rem;
`;

export const WUIInputWrap = styled.div`
  margin: 1.5rem 2rem 0rem;
`

export const MetricSelectionWrap = styled.div`
  margin: 1.5rem 2rem 0rem;
`

export const MetricExtensionWrap = styled.div`
  margin: 1.5rem 2rem 0rem;
`

export const ButtonWrap = styled.div`
  margin: 1rem 0rem;
  margin-left: auto;
`;

export const EvaluationButtonWrap = styled.div`
  margin: 2rem 0rem;
  margin-left: auto;
`

export const EvaluationResultContentWrap = styled.div`
  padding: 2.5rem;
`;