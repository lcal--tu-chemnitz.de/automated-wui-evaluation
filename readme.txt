automated-wui-evaluation.zip is the repository of the project.
It contains the source code as well as the evaluation data for the system performance (metrics computation time).
Further documentations (in files with the name README.md) can be found within the repository.

URL of the repository in Gitlab: https://gitlab.hrz.tu-chemnitz.de